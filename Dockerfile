FROM alpine:3.13

ARG version=3.1.2

RUN apk add ca-certificates wget unzip libc6-compat \
    && wget https://storage.googleapis.com/hak5-dl.appspot.com/cloudc2/firmwares/${version}-stable/c2-${version}.zip --no-cache \
    && unzip c2-${version} \
    && mkdir /app \
    && mv c2-${version}* /app \
    && chmod +x /app/* \
    && apk del wget unzip

COPY run.sh /app

EXPOSE 8080 2022

CMD /bin/ash /app/run.sh
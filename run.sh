#!/bin/bash


# ugly but readable lel
# if variable is not empty
if ! [ -z "$certFile" ]; then
    certFile="-certFile $certFile"
fi

if ! [ -z "$db" ]; then
    db="-db $db"
fi

if ! [ -z "$https" ]; then
    https="-https"
fi

if ! [ -z "$keyFile" ]; then
    keyFile="-keyFile $keyFile"
fi

if ! [ -z "$listenip" ]; then
    listenip="-listenip $listenip"
fi

if ! [ -z "$listenport" ]; then
    listenport="-listenport $listenport"
fi

if ! [ -z "$reverseproxy" ]; then
    reverseproxy="-reverseProxy"
fi

if ! [ -z "$reverseproxyport" ]; then
    reverseproxyport="-reverseProxyPort $reverseproxyport"
fi

if ! [ -z "$sshport" ]; then
    sshport="-sshport $sshport"
fi

hostname="-hostname $hostname"

echo "using following settings:" $hostname $certFile $db $https $keyFile $listenip $listenport $reverseproxy $reverseproxyport $sshport

/app/c2-3.1.2_amd64_linux $hostname $certFile $db $https $keyFile $listenip $listenport $reverseproxy $reverseproxyport $sshport